# Telstra

	Create a universal iOS app using Swift: Yes, develop exerceise using swift

	•	Ingests a json feed https://dl.dropboxusercontent.com/s/2iodh4vg0eortkl/facts.json :   Ingested done

	•	You can use a third party Json parser to parse this if desired. :  Wrote my own JSON parser 
	•	Displays the content (including image, title and description) in a table : Done 
	•	The title in the navbar should be updated from the json : Done 
	•	Each row should be the right height to display its own content and no taller. No content should be clipped. This means some rows will be larger than others. : Done  
	•	Don’t download all images at once, only as needed : Done, used SDWebIMage download image asynchronously 
	•	Refresh function, either a refresh button or use pull down to refresh. : Done 
	•	Should not block UI when loading the data from the json feed. : Done 
	•	Support all iOS versions from the latest back at least 2 versions : Done 
	Guidelines  
	•	Use Git to manage the source code. A clear Git history showing your process is required. : Done 
	•	Structure your code according to industry best practice design patterns : Done, Used MMVM and other helper class 
	•	Do not use any .xib files or Story Boards : Ok, yup not used except default launch image.xib that comes by default when we initiate new project 
	•	Scrolling the table view should be smooth, even as images are downloading and getting  added to the cells : Done 
	•	Support both iPhone and iPad (in both orientations) all devices including iPhoneX : Done 
Additional Requirements 
	•	Use programmatic auto layout using Layout Anchors to layout the cells in the app : Done 
	•	Use the URLSession framework for your service calls : Done 
	•	PleaseuseaTableViewasthecontainer : Done 

