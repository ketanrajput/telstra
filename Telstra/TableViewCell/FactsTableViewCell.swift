//
//  FactsTableViewCell.swift
//  Telstra
//
//  Created by ketan shinde on 04/09/19.
//  Copyright © 2019 ketan shinde. All rights reserved.
//

import UIKit
import SDWebImage

class FactsTableViewCell: UITableViewCell {

    var imgViewFact = UIImageView()
    var lblTitle = UILabel()
    var txtViewDescription = UITextView()
    
    var minHeight: CGFloat?
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setUpCell()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    static var identifier: String {
        return String(describing: self)
    }
    
    func setUpCell() {
        
        contentView.addSubview(imgViewFact)
        imgViewFact.clipsToBounds = true
        imgViewFact.contentMode = .scaleAspectFill
        imgViewFact.layer.cornerRadius = 8.0
        imgViewFact.layer.borderWidth = 1.0
        imgViewFact.layer.borderColor = UIColor.lightGray.cgColor

        imgViewFact.translatesAutoresizingMaskIntoConstraints = false
        imgViewFact.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 8).isActive = true
        imgViewFact.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 8).isActive = true
        imgViewFact.addConstraint(NSLayoutConstraint(item: imgViewFact, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 80))
        imgViewFact.addConstraint(NSLayoutConstraint(item: imgViewFact, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 80))
        
        contentView.addSubview(lblTitle)
        lblTitle.textAlignment = .left
        lblTitle.font = UIFont.boldSystemFont(ofSize: 14.0)
        lblTitle.textColor = .black
        lblTitle.numberOfLines = 1

        lblTitle.setContentHuggingPriority(.required, for: .vertical)
        
        lblTitle.translatesAutoresizingMaskIntoConstraints = false
        lblTitle.leadingAnchor.constraint(equalTo: imgViewFact.trailingAnchor, constant: 8).isActive = true
        lblTitle.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 8).isActive = true
        lblTitle.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: 8).isActive = true
        
        contentView.addSubview(txtViewDescription)
        txtViewDescription.textAlignment = .left
        txtViewDescription.font = UIFont.systemFont(ofSize: 12.0)
        txtViewDescription.isScrollEnabled = false
        txtViewDescription.textColor = .black
        
        txtViewDescription.translatesAutoresizingMaskIntoConstraints = false
        txtViewDescription.leadingAnchor.constraint(equalTo: imgViewFact.trailingAnchor, constant: 8).isActive = true
        txtViewDescription.topAnchor.constraint(equalTo: lblTitle.bottomAnchor, constant: 0).isActive = true
        txtViewDescription.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: 0).isActive = true
        txtViewDescription.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: 0).isActive = true
    }
    
    func loadData(facts: Facts) {
        
        if let title = facts.title {
            lblTitle.text = title
        } else {
            lblTitle.text = "Not Available"
        }
        
        if let desc = facts.description {
            txtViewDescription.text = desc
        } else {
            txtViewDescription.text = "Not Available"
        }
        
        if let imgURL = facts.imageHref {
            imgViewFact.sd_setImage(with: URL.init(string: imgURL), placeholderImage: UIImage(named: "default_icon"))
        } else {
            imgViewFact.image = UIImage(named: "default_icon")
        }
    }
    
    
    override func systemLayoutSizeFitting(_ targetSize: CGSize, withHorizontalFittingPriority horizontalFittingPriority: UILayoutPriority, verticalFittingPriority: UILayoutPriority) -> CGSize {
        
        let size = super.systemLayoutSizeFitting(targetSize, withHorizontalFittingPriority: horizontalFittingPriority, verticalFittingPriority: verticalFittingPriority)
       
        guard let minHeight = minHeight else { return size }
        return CGSize(width: size.width, height: max(size.height, minHeight))
    }
}
