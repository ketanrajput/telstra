//
//  ViewController.swift
//  Telstra
//
//  Created by ketan shinde on 04/09/19.
//  Copyright © 2019 ketan shinde. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    fileprivate var viewModel: ViewModel!
    var tableViewFacts : UITableView = {
        let tableView = UITableView()
        return tableView
    }()
    let tableDelegateAndDataSource = FactsTableProtocol()
    var refreshControl: UIRefreshControl!

    init(viewModel: ViewModel) {
        super.init(nibName: nil, bundle: nil)
        self.viewModel = viewModel
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Controller Life Cycle
    
    override func loadView() {
        super.loadView() // Actually loads and assigns the default view
        self.view.backgroundColor = UIColor.white
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.

        self.view.addSubview(tableViewFacts)
        
        tableViewFacts.translatesAutoresizingMaskIntoConstraints = false
        tableViewFacts.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        tableViewFacts.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        tableViewFacts.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        tableViewFacts.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        createRefreshController()
        
        tableViewFacts.register(FactsTableViewCell.self, forCellReuseIdentifier: FactsTableViewCell.identifier)
        tableViewFacts.delegate = tableDelegateAndDataSource
        tableViewFacts.dataSource = tableDelegateAndDataSource
        
        bindViewModel()
        assignFactsApiWorkToViewModel()
    }
    
    func createRefreshController() {
        DispatchQueue.main.async(execute: {
            
            if (self.refreshControl == nil) {
                self.refreshControl = UIRefreshControl()
            }
            if #available(iOS 10.0, *) {
                self.tableViewFacts.refreshControl = self.refreshControl
            } else {
                self.tableViewFacts.addSubview(self.refreshControl)
            }
            self.refreshControl.addTarget(self, action: #selector(self.assignFactsApiWorkToViewModel), for: .valueChanged)
        })
    }
    
    //MARK: - Helpers
    
    @objc func assignFactsApiWorkToViewModel() {
        viewModel.fetchCountryFacts()
    }
    
    func bindViewModel() {
        viewModel.dataHandler = { [weak self] (title, facts, error) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.refreshControl.endRefreshing()

            if error != nil {
                strongSelf.showAlertWithText(text: error!)
            }
            else {
                strongSelf.title = title!
                strongSelf.tableDelegateAndDataSource.factsList = facts
                strongSelf.tableViewFacts.reloadData()
            }
        }
    }

    //MARK: - UI Things
    func showAlertWithText(text: String) {
        
        let alert = UIAlertController(title: "Error", message: text, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }

}

