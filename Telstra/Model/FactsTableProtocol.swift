//
//  FactsTableProtocol.swift
//  Telstra
//
//  Created by ketan shinde on 04/09/19.
//  Copyright © 2019 ketan shinde. All rights reserved.
//

import Foundation
import UIKit

class FactsTableProtocol: NSObject, UITableViewDelegate, UITableViewDataSource {

    var factsList: [Facts] = []
    
    override init() {
        super.init()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return factsList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: FactsTableViewCell.identifier, for: indexPath) as? FactsTableViewCell {
            cell.selectionStyle = .none
            cell.loadData(facts: factsList[indexPath.row])
            cell.minHeight = 100
            return cell
        }
       return UITableViewCell()
    }
}
