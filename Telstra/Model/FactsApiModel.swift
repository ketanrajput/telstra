//
//  FactsApiModel.swift
//  Telstra
//
//  Created by ketan shinde on 04/09/19.
//  Copyright © 2019 ketan shinde. All rights reserved.
//

import Foundation

// Structs that can be both encode and decode (codable)

struct FactsResponse: Codable {
    
    var title: String?
    var rows: [Facts]?
    
    enum FactsResponseCodingKeys: String, CodingKey {
        case title
        case rows // Naming convention for my own use
    }
    
    
    init(from decoder: Decoder) throws {
        // Handle each key with their respective empty datatypes if not available from service.

        let factsResponseContainer = try decoder.container(keyedBy: FactsResponseCodingKeys.self)
        title = factsResponseContainer.contains(.title) ? try factsResponseContainer.decode(String.self, forKey: .title) : ""
        rows = factsResponseContainer.contains(.rows) ? try factsResponseContainer.decode([Facts].self, forKey: .rows) : []
    }
}

struct Facts: Codable {
    
    var title: String? = nil
    var description: String? = nil
    var imageHref: String? = nil
    
    enum FactsCodingKeys: String, CodingKey {
        case title
        case description
        case imageHref
    }
    
    init(from decoder: Decoder) throws {
        // Handle each key with their respective empty datatypes if not available from service.

        let factsContainer = try decoder.container(keyedBy: FactsCodingKeys.self)
        
        title = factsContainer.contains(.title) ? try factsContainer.decodeIfPresent(String.self, forKey: .title) : ""
        description = factsContainer.contains(.description) ? try factsContainer.decodeIfPresent(String.self, forKey: .description) : ""
        imageHref = factsContainer.contains(.imageHref) ? try factsContainer.decodeIfPresent(String.self, forKey: .imageHref) : ""
    }
}
