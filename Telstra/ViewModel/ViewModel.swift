//
//  ViewModel.swift
//  Telstra
//
//  Created by ketan shinde on 04/09/19.
//  Copyright © 2019 ketan shinde. All rights reserved.
//

import Foundation
import UIKit

class ViewModel: NSObject {
  
    var apiSession = ApiSession()
    var dataHandler:((String?, [Facts], String?) -> Void)?
    var getFacts: [Facts] = [] // for future use
    
    override init() {
        super.init()
    }
    
    func fetchCountryFacts() {
        
        apiSession.urlString = "https://dl.dropboxusercontent.com/s/2iodh4vg0eortkl/facts.json"
        
        apiSession.fetchData(type: FactsResponse.self) { [weak self] (response, error) in
            
            DispatchQueue.main.async(execute: {
                
                guard let strongSelf = self else {
                    return
                }
                if error != nil {
                    strongSelf.dataHandler?(nil, [], error?.localizedDescription)
                }
                else {
                    if let facts = response?.rows {
                        strongSelf.getFacts = facts
                        strongSelf.dataHandler?(response?.title, strongSelf.getFacts, nil)
                    }
                }
            })
        }
    }
}
